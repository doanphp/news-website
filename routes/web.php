<?php

use App\Mail\WelcomeMail;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Sử dụng phương thức GET cho route hiển thị form contact
Route::get('/contact', 'App\Http\Controllers\ContactController@showForm')->name('contact.show');

// Sử dụng phương thức POST cho route gửi email từ form contact
Route::post('/contact/send', 'App\Http\Controllers\ContactController@sendEmail')->name('contact.send');

Route::get('/search-result', 'App\Http\Controllers\SearchController@result')->name('search.result');

Route::get('/logout', 'Auth\HomeController@logout')->name('logout');

Route::resource('comments', 'App\Http\Controllers\CommentController');

Route::post('/comments/store', 'CommentController@store')->middleware('auth.comment');

Route::get('/test-mail', 'App\Http\Controllers\TestMailController@SendMail');

Route::get('/home', 'App\Http\Controllers\Homecontroller@home')->name('home');
Route::get('/', function () {
    return view('frontend.index');
});

//Frontend routes
Route::get('contact', 'App\Http\Controllers\Homecontroller@contact')->name('contact');

Route::get('about', 'App\Http\Controllers\Homecontroller@about')->name('about');

Route::get('post-detail/{slug}', 'App\Http\Controllers\Homecontroller@single_post')->name('single-post');

Route::get('single', 'App\Http\Controllers\Homecontroller@single')->name('single');

Route::get('categories/{slug}', 'App\Http\Controllers\Homecontroller@post_categories')->name('post_categories');

Route::group(['middleware'=>'auth'], function () {
});
//Admin routes
Route::group(['middleware'=>['auth','admin']], function () {

    Route::get('/admin/category', 'App\Http\Controllers\Categorycontroller@index')->name('category.index');

    Route::get('/admin/category/create', 'App\Http\Controllers\Categorycontroller@create')->name('category.create');

    Route::post('/admin/category/store', 'App\Http\Controllers\Categorycontroller@store')->name('category.store');

    Route::post('/admin/category/update', 'App\Http\Controllers\Categorycontroller@update')->name('category.update');

    Route::get('/admin/category/edit/{id}', 'App\Http\Controllers\Categorycontroller@edit')->name('category.edit');

    Route::delete('/admin/category/delete' , 'App\Http\Controllers\Categorycontroller@delete')->name('category.delete');

});

//Post routes

Route::get('/admin/posts/delete/{id}', 'App\Http\Controllers\Postcontroller@delete')->name('posts.delete');

Route::resource('posts', 'App\Http\Controllers\Postcontroller');



Route::middleware([
    'auth:sanctum',
    config('jetstream.auth_session'),
    'verified'
])->group(function () {
    Route::get('/dashboard', function () {
        return view('dashboard');
    })->name('dashboard');
});
