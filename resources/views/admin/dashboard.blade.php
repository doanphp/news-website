@extends('admin.layouts.master')
@section('content')
    
<div class="pagetitle">
    <h1>Trang điều khiển</h1>
    <nav>
      <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{route('home')}}">Trang chủ</a></li>
        <li class="breadcrumb-item active">Trang điều khiển</li>
      </ol>
    </nav>
  </div><!-- End Page Title -->

@endsection