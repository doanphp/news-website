@extends('frontend.layouts.master')
@push('meta')
    <meta content="" name="description">
    <meta content="" name="keywords">
@endpush
@section('content')

    <section id="hero-slider" class="hero-slider">
        <div class="container-md" data-aos="fade-in">
            <div class="row">
                <div class="col-12">
                    <div class="swiper sliderFeaturedPosts">
                        <div class="swiper-wrapper">
                            @php
                                $posts = App\Models\Post::orderBy('views', 'DESC')->take(5)->get();
                                $count = 1;
                            @endphp
                            @foreach ($posts as $item)
                                <div class="swiper-slide">
                                    <a href="{{ route('single-post', $item->slug) }}" class="img-bg d-flex align-items-end"
                                        style="background-image: url('{{ asset('uploads/post/' . $item->photo) }}');">
                                        <div class="img-bg-inner">
                                            <h2>{{ $item->title }}</h2>
                                            <p>{{ $item->excerpt }}</p>
                                        </div>
                                    </a>
                                </div>
                                @php
                                    $count++;
                                @endphp
                            @endforeach
                        </div>
                        <div class="custom-swiper-button-next">
                            <span class="bi-chevron-right"></span>
                        </div>
                        <div class="custom-swiper-button-prev">
                            <span class="bi-chevron-left"></span>
                        </div>

                        <div class="swiper-pagination"></div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    {{-- Phần tin mới và tin nóng --}}

    <section id="posts" class="posts">
        <div class="container" data-aos="fade-up">
            <div class="section-header d-flex justify-content-between align-items-center mb-5">
                <h2>Tin mới</h2>
            </div>
            <div class="row g-5">
                <div class="col-lg-4">

                    @php
                        $posts = App\Models\Post::latest()->take(6)->get();
                    @endphp
                    @foreach ($posts as $index => $post)
                        @if ($index < 3)
                            <div class="post-entry-1 lg">
                                <a href="{{ route('single-post', $post->slug) }}"><img
                                        src="{{ asset('uploads/post/' . $post->photo) }}" alt=""
                                        class="img-fluid"></a>
                                <div class="post-meta"><span class="date">{{ $post->category->name }}</span> <span
                                        class="mx-1">&bullet;</span> <span>{{ format_date($post->created_at) }}</span>
                                </div>
                                <h2 style="font-size: 18px;"><a
                                        href="{{ route('single-post', $post->slug) }}">{{ $post->title }}</a></h2>
                                <p class="mb-4 d-block" style="font-size: 16px;">{{ $post->excerpt }}</p>
                                <div class="d-flex align-items-center author">
                                    <div class="photo"><img src="{{ asset('assets/img/person-1.jpg') }}" alt=""
                                            class="img-fluid"></div>
                                    <div class="name">
                                        <h3 class="m-0 p-0" style="font-size: 16px;">{{ $post->author }}</h3>
                                    </div>
                                </div>
                            </div>
                        @endif
                    @endforeach
                </div>

                <div class="col-lg-8">
                    <div class="row g-5">
                        <div class="col-lg-6 border-start custom-border">
                            @foreach ($posts as $index => $post)
                                @if ($index >= 3)
                                    <div class="post-entry-1">
                                        <a href="{{ route('single-post', $post->slug) }}"><img
                                                src="{{ asset('uploads/post/' . $post->photo) }}" alt=""
                                                class="img-fluid"></a>
                                        <div class="post-meta"><span class="date">{{ $post->category->name }}</span>
                                            <span class="mx-1">&bullet;</span>
                                            <span>{{ format_date($post->created_at) }}</span></div>
                                        <h2><a href="{{ route('single-post', $post->slug) }}">{{ $post->title }}</a></h2>
                                    </div>
                                @endif
                            @endforeach
                        </div>

                        <div class="col-lg-6">
                            <div class="trending text-end">
                                <h3>Tin nóng</h3>
                                <ul class="trending-post">
                                    @php
                                        // Dùng orderBy() để sắp xếp, DESC tham số giảm dần
                                        $posts = App\Models\Post::orderBy('views', 'DESC')->take(10)->get();
                                        $count = 1;
                                    @endphp

                                    @foreach ($posts as $post)
                                        <li>
                                            <a href="{{ route('single-post', $post->slug) }}">
                                                <span class="number">{{ $count }}</span>
                                                <h3>{{ $post->title }}</h3>
                                                <span class="author">{{ $post->author }}</span>
                                            </a>
                                        </li>
                                        @php
                                            $count++;
                                        @endphp
                                    @endforeach
                                </ul>
                            </div>
                            <br><br>
                            @php
                            $categories = \App\Models\Category::where('status','Active')->latest()->take(8)->get();
                        @endphp
                          <div class="aside-block">
                            <h3 class="aside-title">Thể loại tin</h3>
                            <ul class="aside-links list-unstyled">
                                @foreach ($categories as $category)
                                    <li><a href="{{ route('post_categories',$category->slug) }}"><i class="bi bi-chevron-right"></i> {{ $category->name }}</a></li>
                                @endforeach
                            </ul>
                          </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    @php
        $categories = App\Models\Category::where('status', 'Active')->latest()->get();
        // dd($categories);
    @endphp
    @foreach ($categories as $category)
        {{-- {{ dd($category->id)}}  --}}

        <section class="category-section">
            <div class="container" data-aos="fade-up">

                <div class="section-header d-flex justify-content-between align-items-center mb-5">
                    <h2>{{ $category->name }}</h2>
                    <div><a href="{{ route('post_categories', $category->slug) }}" class="more"> Xem thêm
                            {{ $category->name }}</a></div>
                </div>

                <div class="row">
                    <div class="col-md-9">
                        <div class="row">
                            @php
                                $posts = App\Models\Post::where('status', 'Active')
                                    ->where('category_id', $category->id)
                                    ->latest()
                                    ->take(3)
                                    ->get();
                            @endphp
                            @foreach ($posts as $item)
                                <div class="col-lg-4">
                                    <div class="post-entry-1 border-bottom">
                                        <a href="{{ route('single-post', $item->slug) }}"><img
                                                src="{{ asset('uploads/post/' . $item->photo) }}" alt=""
                                                class="img-fluid"></a>
                                        <div class="post-meta"><span class="date">{{ $item->category->name }}</span>
                                            <span class="mx-1">&bullet;</span>
                                            <span>{{ format_date($item->created_at) }}</span>
                                        </div>
                                        <h2 class="mb-2"><a
                                                href="{{ route('single-post', $item->slug) }}">{{ $item->title }}</a>
                                        </h2>
                                        <span class="author mb-3 d-block">{{ $item->user->name }}</span>
                                        <p class="mb-4 d-block">{{ text_short($item->short_description, 500) }}</p>
                                    </div>
                                </div>
                            @endforeach

                        </div>
                    </div>

                    <div class="col-md-3">

                        @foreach ($posts as $item)
                            <div class="post-entry-1 border-bottom">
                                <div class="post-meta"><span class="date">{{ $item->category->name }}</span> <span
                                        class="mx-1">&bullet;</span> <span>{{ format_date($item->created_at) }}</span>
                                </div>
                                <h2 class="mb-2"><a
                                        href="{{ route('single-post', $item->slug) }}">{{ $item->title }}</a></h2>
                                <span class="author mb-3 d-block">{{ $item->user->name }}</span>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </section>
    @endforeach
@endsection
