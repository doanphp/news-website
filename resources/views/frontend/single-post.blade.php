@extends('frontend.layouts.master')
@push('meta')
    <title>
        {{ $post->title }}</title>

    <meta content="{{ text_short($post->description, 500) }}" name="description">
    <meta content="{{ make_keyword($post->title) }}" name="keywords">
    <meta content="{{ $post->title }}" name="title">
@endpush

@section('content')

    <section class="single-post-content">
        <div class="container">
            <div class="row">
                <div class="col-md-9 post-content" data-aos="fade-up">

                    <!-- ======= Single Post Content ======= -->
                    <div class="single-post">
                        <div class="post-meta"><span class="date">{{ $post->category->name }}</span> <span
                                class="mx-1">&bullet;</span> <span>{{ format_date($post->created_at) }}</span></div>
                        <h1 class="mb-5">{{ $post->title }}</h1>
                        <div class="post-views"
                            style="background-color: #f0f0f0; border-radius: 10px; padding: 10px; display: flex; align-items: center;">
                            <div>
                                <h5>Người đăng: {{ $post->user->name }}</h5>
                            </div>
                        </div>
                        <br>
                        <h4 class="bold-text">{{ $post->short_description }}</h4>

                        <figure class="my-4">
                            <img src="{{ asset('uploads/post/' . $post->photo) }}" alt="" class="img-fluid">

                        </figure>

                        <div>
                            {!! $post->description !!}
                        </div>
                    </div>
                    <div>
                        <div class="post-views"
                            style="background-color: #f0f0f0; border-radius: 10px; padding: 10px; display: flex; align-items: center;">
                            <div class="view-count">
                                <h5 class="your-class-name"><i class="far fa-eye"></i> Lượt xem: {{ $post->views }}</h5>
                            </div>
                            <div style="margin-left: auto;">
                                <p>Bài đăng lúc: {{ format_date($post->created_at) }}</p>
                            </div>
                        </div>
                    </div>
                    <br><br>
                    <h3>Các Bình luận</h3>
                    @foreach ($post->comments as $comment)
                    <div class="comment">
                        {{-- Kiểm tra nếu có người dùng được liên kết với bình luận --}}
                        @if ($comment->user)
                            {{-- Hiển thị tên người dùng và nội dung bình luận --}}
                            
                            <div class="comment-user">
                               
                                <strong>{{ $comment->user->name }}</strong> đã bình luận: {{ $comment->content }}
                            </div>
                        @else
                            {{-- Hiển thị thông báo nếu không có người dùng --}}
                            <div class="comment-user">
                                Người dùng không xác định đã bình luận: {{ $comment->content }}
                            </div>
                        @endif
                    </div>              
                    
                    @endforeach
                    <br><br>
                    <!-- ======= Comments Form ======= -->
                    @if (Auth::check())
                        <!-- Form bình luận -->
                        <form action="{{ route('comments.store') }}" method="post">
                            @csrf
                            <input type="hidden" name="post_id" value="{{ $post->id }}">
                            <div class="col-12 mb-3">
                                <h3 for="comment-message">Viết Bình luận</h3>
                                <textarea name="content" class="form-control" id="comment-message" placeholder="Bình luận của bạn" cols="30"
                                    rows="10"></textarea>
                            </div>
                            <div class="col-12">
                                <input type="submit" class="btn btn-primary" value="Bình Luận">
                            </div>
                        </form>
                    @else
                        <p>Bạn cần đăng nhập để bình luận.</p>
                    @endif
                </div>

                <div class="col-md-3">
                    @include('frontend.inc.sidebar')
                </div>
            </div>
        </div>
    </section>
