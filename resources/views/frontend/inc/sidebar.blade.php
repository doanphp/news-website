<!-- ======= Sidebar ======= -->
<div class="aside-block">

    <ul class="nav nav-pills custom-tab-nav mb-4" id="pills-tab" role="tablist">
        <li class="nav-item" role="presentation">
            <button class="nav-link active" id="pills-popular-tab" data-bs-toggle="pill"
                data-bs-target="#pills-popular" type="button" role="tab" aria-controls="pills-popular"
                aria-selected="true">Tin Phổ biến</button>
        </li>
        <li class="nav-item" role="presentation">
            <button class="nav-link" id="pills-latest-tab" data-bs-toggle="pill" data-bs-target="#pills-latest"
                type="button" role="tab" aria-controls="pills-latest" aria-selected="false">Tin cũ</button>
        </li>
    </ul>

    <div class="tab-content" id="pills-tabContent">

        <div class="tab-pane fade show active" id="pills-popular" role="tabpanel"
            aria-labelledby="pills-popular-tab">
            @php
                $popular_posts = App\Models\Post::orderBy('views', 'DESC')->take(5)->get();
            @endphp
            @foreach ($popular_posts as $item)
                <div class="post-entry-1 border-bottom">
                    <div class="post-meta"><span class="date">{{ $item->category->name }}</span> <span
                            class="mx-1">&bullet;</span> <span>{{ format_date($item->created_at) }}</span></div>
                    <h2 class="mb-2"><a href="{{ route('single-post', $item->slug) }}">{{ $item->title }}</a></h2>
                    @if(isset($item->user))
                        <span class="author mb-3 d-block">Người đăng: {{ $item->user->name }}</span>
                    @endif
                </div>
            @endforeach
        </div>

        <div class="tab-pane fade" id="pills-latest" role="tabpanel" aria-labelledby="pills-latest-tab">
            @php
                $latest_posts = App\Models\Post::orderBy('created_at', 'ASC')->take(5)->get();
            @endphp
            @foreach ($latest_posts as $item)
                <div class="post-entry-1 border-bottom">
                    <div class="post-meta"><span class="date">{{ $item->category->name }}</span> <span
                            class="mx-1">&bullet;</span> <span>{{ format_date($item->created_at) }}</span></div>
                    <h2 class="mb-2"><a href="{{ route('single-post', $item->slug) }}">{{ $item->title }}</a></h2>
                    @if(isset($item->user))
                        <span class="author mb-3 d-block">Người đăng: {{ $item->user->name }}</span>
                    @endif
                </div>
            @endforeach
        </div>

    </div>
</div>

@php
    $categories = \App\Models\Category::where('status', 'Active')->latest()->take(8)->get();
@endphp
<div class="aside-block">
    <h3 class="aside-title">Thể loại tin</h3>
    <ul class="aside-links list-unstyled">
        @foreach ($categories as $category)
            <li><a href="{{ route('post_categories', $category->slug) }}"><i
                        class="bi bi-chevron-right"></i>{{ $category->name }}</a></li>
        @endforeach
    </ul>
</div>
