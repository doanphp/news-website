<header id="header" class="header d-flex align-items-center fixed-top">
    <div class="container-fluid container-xl d-flex align-items-center justify-content-between">
        <a href="{{ url('/') }}" class="logo d-flex align-items-center">
            <h1>Web News</h1>
        </a>

        <nav id="navbar" class="navbar">
            <ul>
                <li><a href="{{ url('/') }}">Blog</a></li>
                {{-- <li><a href="{{route('single')}}">Tin Vắn</a></li> --}}
                <li class="dropdown"><a href="category.html"><span>Thể loại</span> <i
                            class="bi bi-chevron-down dropdown-indicator"></i></a>
                    <ul>
                        <li><a href="search-result.html">Search Result</a></li>
                        <li><a href="#">Drop Down 1</a></li>
                        <li class="dropdown"><a href="#"><span>Deep Drop Down</span> <i
                                    class="bi bi-chevron-down dropdown-indicator"></i></a>
                            <ul>
                                <li><a href="#">Deep Drop Down 1</a></li>
                                <li><a href="#">Deep Drop Down 2</a></li>
                                <li><a href="#">Deep Drop Down 3</a></li>
                                <li><a href="#">Deep Drop Down 4</a></li>
                                <li><a href="#">Deep Drop Down 5</a></li>
                            </ul>
                        </li>
                        <li><a href="#">Drop Down 2</a></li>
                        <li><a href="#">Drop Down 3</a></li>
                        <li><a href="#">Drop Down 4</a></li>
                    </ul>
                </li>

                <li><a href="{{ route('about') }}">About</a></li>
                <li><a href="{{ route('contact') }}">Contact</a></li>
            </ul>
        </nav>



        <div class="position-relative">
            <a href="https://www.facebook.com/ltuananh.orsted" class="mx-2"><span class="bi-facebook"></span></a>
            <a href="https://twitter.com/?lang=vi" class="mx-2"><span class="bi-twitter"></span></a>
            <a href="https://www.instagram.com/im_ltuananh/?hl=vi" class="mx-2"><span class="bi-instagram"></span></a>

            <a href="#" class="mx-2 js-search-open"><span class="bi-search"></span></a>
            <i class="bi bi-list mobile-nav-toggle"></i>

            <div class="search-form-wrap js-search-form-wrap">
                <form action="{{ route('search.result') }}" method="GET" class="search-form" id="search-form">
                    <span class="icon bi-search"></span>
                    <input type="text" name="search" id="search-input" placeholder="Search" class="form-control">
                    <button type="submit" class="btn js-search-close"><span class="bi-x"></span></button>
                </form>
            </div>
            
            <script>
                // Bắt sự kiện keypress trên trường input
                document.getElementById("search-input").addEventListener("keypress", function(event) {
                    // Kiểm tra nếu phím nhấn là Enter (keyCode 13)
                    if (event.keyCode === 13) {
                        // Ngăn chặn hành động mặc định của form (chặn việc tải lại trang)
                        event.preventDefault();
            
                        // Lấy giá trị từ ô tìm kiếm
                        var searchTerm = document.getElementById("search-input").value;
            
                        // Chuyển hướng đến trang kết quả tìm kiếm với tham số search
                        window.location.href = "{{ route('search.result') }}?search=" + searchTerm;
                    }
                });
            </script>
            
                      


        </div>
        <div class="position-relative">
            <div class="hidden fixed top-0 right-0 px-6 py-4 sm:block">
                <div class="btn-group">
                    @if (Route::has('login'))
                        @auth
                            @if (auth()->check())
                                <p>Xin chào, <a href="{{ url('/dashboard') }}"
                                        class="text-sm text-gray-700 dark:text-gray-500 underline">{{ auth()->user()->name }}</a>
                                </p>
                            @endif
                        @else
                            <a href="{{ route('login') }}" class="btn btn-primary">Đăng nhập</a>

                            @if (Route::has('register'))
                                <a href="{{ route('register') }}" class="btn btn-secondary">Đăng kí</a>
                            @endif
                        @endauth
                    @endif
                </div>
            </div>
        </div>


    </div>

</header>
