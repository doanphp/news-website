<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AdminAuthMiddleware
{
    public function handle(Request $request, Closure $next)
    {
        
           if(Auth::user()->user_type =='admin'){  
               // return view('admin.dashboard');
                return $next($request);
           }  else{
                return redirect()->route('home');
           }        
        
    }
}
