<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\Post;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
    class PostController extends Controller
    {
        
        public function index()
        {
            $models = Post::with('category')->paginate(5);
            return view('admin.post.index',compact('models'));
        }

    
        public function create()
        {
            $models = Category::where('status', 'Active')->get();
            return view('admin.post.create',compact('models'));
        }

        public function store(Request $request)
        {
        $request->validate([
                'title' => ['required','unique:posts'],
                'category_id' => ['required'],
                'status' => ['required'],
                'short_description' => ['required'],
                'description' => ['required'],
                'photo' => ['required']
        ]);
        
        
    // composer require intervention/image
    // thư viện hình ảnh Intervention Image
        $model = new Post;
        $model->title = $request->title;
        $model->category_id = $request->category_id;
        $model->added_by = Auth::user()->id;
        $model->status = $request->status;
        $model->short_description = $request->short_description;
        $model->description = $request->description;
        $model->slug = Str::slug($request->title);

        if($request->hasFile('photo')){
                $file = $request->file('photo');
                $ext = $file->extension() ? : 'png';
                $photo = Str::random(10) . '.' . $ext;

                // resize image

                $path = public_path(). '/uploads/post/';
                $resize = Image::make($file->getRealPath());
                $resize->resize(900,570);
                $resize->save($path.'/'.$photo);

                $model->photo = $photo;
        }
        $model->save();
            return redirect()->route('posts.index')->with('message','Post Insert Successfully');      
        }

    
        public function show(Post $post)
        {
            dd($post);
        }
        public function edit($id)
        {
            $model = Post::findOrFail($id);
            $models = Category::where('status','Active')->get();
            return view('admin.post.edit',compact('models','model'));
        }

    
        public function update(Request $request, Post $post)
        {
            //dd($request->all());
        $model = $post;
        $model->title = $request->title;
        $model->category_id = $request->category_id;
        $model->status = $request->status;
        $model->short_description = $request->short_description;
        $model->description = $request->description;
        $model->slug = Str::slug($request->title);

        if($request->hasFile('photo')){
                $file = $request->file('photo');
                $ext = $file->extension() ? : 'png';
                $photo = Str::random(10) . '.' . $ext;

                // resize image

                $path = public_path(). '/uploads/post/';
                $resize = Image::make($file->getRealPath());
                $resize->resize(900,570);

                // Old Photo Delete 
                if($request->old_photo){
                    $oldphoto = public_path(). '/uploads/post/'.$request->old_photo;
                    unlink($oldphoto);
                }
                $resize->save($path.'/'.$photo);

                $model->photo = $photo;
        }

        $model->save();

            return redirect()->route('posts.index')->with('message','Post Update Successfully');
        }
        
        public function destroy(Post $post)
        {
            dd($post);
        }
        public function delete($id)
        {
            $model = Post::findOrFail($id);
            if($model->photo){
                $oldphoto = public_path(). '/uploads/post/'.$model->photo;
                unlink($oldphoto);
            }
            $model->delete();
        }

    
        
    }

