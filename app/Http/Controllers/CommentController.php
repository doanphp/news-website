<?php

namespace App\Http\Controllers;
use App\Models\Comment;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
class CommentController extends Controller
{
    public function store(Request $request)
    {
        // Kiểm tra xem người dùng đã đăng nhập hay chưa
        if (Auth::check()) {
            $request->validate([
                'content' => 'required',
                'post_id' => 'required|exists:posts,id',
            ]);
    
            $comment = new Comment();
            $comment->user_id = Auth::id(); // Lấy id của người dùng đã đăng nhập
            $comment->post_id = $request->post_id;
            $comment->content = $request->content;
            $comment->save();
    
            return redirect()->back()->with('success', 'Bình luận đã được thêm thành công.');
        } else {
            // Nếu người dùng chưa đăng nhập, chuyển hướng về trang đăng nhập và hiển thị thông báo
            return redirect()->route('login')->with('error', 'Bạn cần đăng nhập để bình luận.');
        }
    }
}