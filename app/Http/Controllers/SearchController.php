<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Post;

class SearchController extends Controller
{
    public function result(Request $request)
    {
        $searchTerm = $request->input('search');
        // Truy vấn cơ sở dữ liệu sử dụng Eloquent
        $posts = Post::where('title', 'LIKE', '%' . $searchTerm . '%')->get();
        // Trả về view hiển thị kết quả tìm kiếm
        return view('frontend.search-result', ['posts' => $posts]);
    }
}

