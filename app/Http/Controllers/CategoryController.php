<?php

namespace App\Http\Controllers;
use App\Models\Post;
use App\Models\Category;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
class CategoryController extends Controller
{
    public function index(){
        $models = Category::all();
        return view('admin.category.index',compact('models'));
    }
    public function create(){
        return view('admin.category.create');
    }
    public function store(Request $request){
       $model = new Category();
       $model->name = $request->name;
       $model->status = $request->status;
       $model->slug = Str::slug($request->name);
       $model->save();

       return redirect()->route('category.index')->with('message','Thêm thể loại tin thành công');
    }

    public function update(Request $request){
        $model = Category::findOrFail($request->id);
        $model->name = $request->name;
        $model->status = $request->status;
        $model->slug = Str::slug($request->name);
        $model->save();
 
        return redirect()->route('category.index')->with('message','Cập nhật thể loại tin thành công');
     }
     
    public function edit($id){
        $model = Category::findOrFail($id);
        return view('admin.category.edit',compact('model'));
     }

     public function delete(Request $request){
        $model = Category::findOrFail($request->id);
        $posts = Post::where('category_id',$request->id)->get()->count();
        if($posts > 0){
            return response()->json([
                'error' => 'Đang có bài viết gắn thẻ thể loại này. Không thể xóa!'
            ]);
        }
        $model->delete();
        return response()->json([
            'success' => 'Xóa thể loại tin thành công'
        ]);    
    }
}